resource "google_dns_managed_zone" "platinumleo-net" {
    dns_name      = "platinumleo.net."
    name          = "platinumleo-net"
    project       = "personal-space-266520"
    visibility    = "public"
}

