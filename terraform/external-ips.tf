resource "google_compute_global_address" "app-cluster-ingress" {
    address_type       = "EXTERNAL"
    name               = "app-cluster-ingress"
}

# resource "google_compute_global_address" "app-cluster-devops-ip" {
#     address_type = "EXTERNAL"
#     name = "app-cluster-devops-ip"
# }
