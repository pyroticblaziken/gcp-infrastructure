data "google_client_config" "default" {}

resource "google_container_cluster" "app" {
    enable_autopilot            = true
    enable_kubernetes_alpha     = false
    enable_legacy_abac          = false
    enable_tpu                  = false
    initial_node_count          = 0
    location                    = "us-central1"
    logging_service             = "logging.googleapis.com/kubernetes"
    monitoring_service          = "monitoring.googleapis.com/kubernetes"
    name                        = "autopilot-app-cluster"
    network                     = "projects/personal-space-266520/global/networks/default"
    networking_mode             = "VPC_NATIVE"
    node_locations              = [
        "us-central1-a",
        "us-central1-b",
        "us-central1-c",
        "us-central1-f",
    ]
    node_version                = "1.21.5-gke.1302"
    project                     = "personal-space-266520"
    resource_labels             = {}
    subnetwork                  = "projects/personal-space-266520/regions/us-central1/subnetworks/default"

    addons_config {

        horizontal_pod_autoscaling {
            disabled = false
        }

        http_load_balancing {
            disabled = false
        }
    }

    database_encryption {
        state = "DECRYPTED"
    }

    default_snat_status {
        disabled = false
    }

    logging_config {
        enable_components = [
            "SYSTEM_COMPONENTS",
            "WORKLOADS",
        ]
    }

    monitoring_config {
        enable_components = [
            "SYSTEM_COMPONENTS",
        ]
    }

    node_config {
        disk_size_gb      = 100
        disk_type         = "pd-standard"
        guest_accelerator = []
        image_type        = "COS_CONTAINERD"
        labels            = {}
        local_ssd_count   = 0
        machine_type      = "e2-standard-2"
        metadata          = {
            "disable-legacy-endpoints" = "true"
        }
        oauth_scopes      = [
            "https://www.googleapis.com/auth/devstorage.read_only",
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
            "https://www.googleapis.com/auth/service.management.readonly",
            "https://www.googleapis.com/auth/servicecontrol",
            "https://www.googleapis.com/auth/trace.append",
        ]
        preemptible       = false
        service_account   = "default"
        tags              = []
        taint             = []

        shielded_instance_config {
            enable_integrity_monitoring = true
            enable_secure_boot          = true
        }

        workload_metadata_config {
            mode          = "GKE_METADATA"
        }
    }

    release_channel {
        channel = "REGULAR"
    }

    timeouts {}

    vertical_pod_autoscaling {
        enabled = true
    }
}