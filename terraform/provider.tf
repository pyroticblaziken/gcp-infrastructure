provider "google" {
  credentials = "${file("./creds/serviceaccount.json")}"
  project = "personal-space-266520"
  region  = "us-central1"
}
