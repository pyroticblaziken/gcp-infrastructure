terraform {
  backend "gcs" {
    credentials = "./creds/serviceaccount.json"
    bucket = "lyons-terraform-default"
    prefix = "default/state"
  }
}
