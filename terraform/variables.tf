variable "project_id" {
  description = "The project ID to create resources in"
  default = "personal-space-266520"
}