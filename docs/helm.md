# Helm Notes

## Repos

### Public 

stable                  https://charts.helm.sh/stable
chartmuseum             https://chartmuseum.github.io/charts
emissary-ingress        https://app.getambassador.io
jenkinsci               https://charts.jenkins.io
prometheus-community    https://prometheus-community.github.io/helm-charts

### Internal/Private

platinum-leo            https://chartmuseum.platinumleo.net
