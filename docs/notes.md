# Notes

- Deploy Chart Museum
  - https://github.com/helm/chartmuseum
  - https://github.com/chartmuseum/charts/tree/main/src/chartmuseum


- Deploy Emissary
  - https://www.getambassador.io/docs/emissary/latest/topics/install/helm/
  - https://www.getambassador.io/docs/emissary/latest/tutorials/getting-started/


https://www.getambassador.io/docs/emissary/latest/topics/running/ambassador-with-gke/
https://cloud.google.com/kubernetes-engine/docs/tutorials/http-balancer
https://www.google.com/search?client=firefox-b-1-d&q=ambassador+mapping+across+namespaces
https://www.getambassador.io/docs/edge-stack/latest/topics/using/intro-mappings/
https://www.getambassador.io/docs/emissary/latest/topics/using/mappings/

## Getting HTTPS to work

- https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs
- https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-features#cannot_enable_https_redirects_with_the_v1_ingress_naming_scheme
- https://cloud.google.com/kubernetes-engine/docs/concepts/ingress-xlb
- https://cloud.google.com/load-balancing/docs/ssl-certificates/troubleshooting

## Ingress things

- https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-multi-ssl
- https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-features#configuring_ingress_features
